# Notes

## REPL Prep

1. Download and install [Leiningen](http://leiningen.org).
2. Open a terminal window
3. Run `lein repl`
4. You should see something similar to: 

``` clojure 
nREPL server started on port 64988 on host 127.0.0.1 - nrepl://127.0.0.1:64988
REPL-y 0.3.7, nREPL 0.2.12
Clojure 1.8.0
Java HotSpot(TM) 64-Bit Server VM 1.8.0_60-b27
    Docs: (doc function-name-here)
          (find-doc "part-of-name-here")
  Source: (source function-name-here)
 Javadoc: (javadoc java-object-or-class-here)
    Exit: Control+D or (exit) or (quit)
 Results: Stored in vars *1, *2, *3, an exception in *e
```

## Try out the REPL 

By convention in these examples, I will have Clojure code in the first formatted block and the REPL output in the second.   

(I recommend that you type the code into the REPL rather than copy and paste, that seems to activate the brain more.)   


Use tab-completion freely in Leiningen - it will offer suggested matches for namespaces and function names.

Enter `*` at the REPL and hit TAB to see Clojure's global settings:

```shell
*                           *'                          *1                          *2
*3                          *agent*                     *allow-unresolved-vars*     *assert*
*clojure-version*           *command-line-args*         *compile-files*             *compile-path*
*compiler-options*          *data-readers*              *default-data-reader-fn*    *e
*err*                       *file*                      *flush-on-newline*          *fn-loader*
*in*                        *math-context*              *ns*                        *out*
*print-dup*                 *print-length*              *print-level*               *print-meta*
*print-readably*            *read-eval*                 *source-path*               *suppress-read*
*unchecked-math*            *use-context-classloader*   *verbose-defrecords*        *warn-on-reflection*
```

Let's look up the default way to run unit-tests:

``` clojure
(find-doc "unit test")
```

``` clojure
-------------------------
clojure.test
  A unit testing framework.

   ASSERTIONS

   The core of the library is the "is" macro, which lets you make
   assertions of any arbitrary expression:

   (is (= 4 (+ 2 2)))
   (is (instance? Integer 256))
   (is (.startsWith "abcde" "ab"))

   You can type an "is" expression directly at the REPL, which will
   print a message if it fails.

       user> (is (= 5 (+ 2 2)))

       FAIL in  (:1)
       expected: (= 5 (+ 2 2))
         actual: (not (= 5 4))
       false

   The "expected:" line shows you the original expression, and the
   "actual:" shows you what actually happened.  In this case, it
   shows that (+ 2 2) returned 4, which is not = to 5.  Finally, the
   "false" on the last line is the value returned from the
   expression.  The "is" macro always returns the result of the
   inner expression.

   There are two special assertions for testing exceptions.  The
   "(is (thrown? c ...))" form tests if an exception of class c is
   thrown:

   (is (thrown? ArithmeticException (/ 1 0)))

   "(is (thrown-with-msg? c re ...))" does the same thing and also
   tests that the message on the exception matches the regular
   expression re:

   (is (thrown-with-msg? ArithmeticException #"Divide by zero"
                         (/ 1 0)))

   DOCUMENTING TESTS

   "is" takes an optional second argument, a string describing the
   assertion.  This message will be included in the error report.

   (is (= 5 (+ 2 2)) "Crazy arithmetic")
```

And the docs go on.. I'm an impatient developer, how do I run tests?

Looks like the namespace is `clojure.test`; enter that and hit TAB:

``` clojure
clojure.test
```

``` shell
clojure.test/*initial-report-counters*   clojure.test/*load-tests*                clojure.test/*report-counters*
clojure.test/*stack-trace-depth*         clojure.test/*test-out*                  clojure.test/*testing-contexts*
clojure.test/*testing-vars*              clojure.test/are                         clojure.test/assert-any
clojure.test/assert-expr                 clojure.test/assert-predicate            clojure.test/compose-fixtures
clojure.test/deftest                     clojure.test/deftest-                    clojure.test/do-report
clojure.test/file-position               clojure.test/function?                   clojure.test/get-possibly-unbound-var
clojure.test/inc-report-counter          clojure.test/is                          clojure.test/join-fixtures
clojure.test/report                      clojure.test/run-all-tests               clojure.test/run-tests
clojure.test/set-test                    clojure.test/successful?                 clojure.test/test-all-vars
clojure.test/test-ns                     clojure.test/test-var                    clojure.test/test-vars
clojure.test/testing                     clojure.test/testing-contexts-str        clojure.test/testing-vars-str
clojure.test/try-expr                    clojure.test/use-fixtures                clojure.test/with-test
clojure.test/with-test-out
```

Looks like `clojure.test/run-all-tests` or `clojure.test/run-tests` are what I'll want.

Before we jump into TDD ... let's try a few simple things.

Adding some numbers:

``` clojure
(+ 3 4)
```

``` shell
7
```

We can create a list that is a range of numbers:

``` clojure
(range 0 9)
```

``` shell
(0 1 2 3 4 5 6 7 8)
```

And count how many elements it contains:

``` clojure
(count (range 0 9))
```

``` shell
9
```

Taking a hint from the REPL startup, we can look up the built-in function documentation for `count`:

``` clojure
(doc count)
```

``` shell
-------------------------
clojure.core/count
([coll])
  Returns the number of items in the collection. (count nil) returns
  0.  Also works on strings, arrays, and Java Collections and Maps
nil
```

Notice that `nil` was returned - every Clojure expression evaluates to a value.  

Lisp expression: `(function-name value)`

By the way, if the parentheses start bothering you, try mentally shifting the function name left:
`function-name(value)`

For every Clojure library on the classpath that bundles its source in the JAR 
(which is most Clojure libraries), you can look up the source via the REPL using the `source` function.

Let's take a look at the source for the `range` function:

``` clojure
(source range)
```

``` clojure 
 (source range)
(defn range
  "Returns a lazy seq of nums from start (inclusive) to end
  (exclusive), by step, where start defaults to 0, step to 1, and end to
  infinity. When step is equal to 0, returns an infinite sequence of
  start. When start is equal to end, returns empty list."
  {:added "1.0"
   :static true}
  ([]
   (iterate inc' 0))
  ([end]
   (if (instance? Long end)
     (clojure.lang.LongRange/create end)
     (clojure.lang.Range/create end)))
  ([start end]
   (if (and (instance? Long start) (instance? Long end))
     (clojure.lang.LongRange/create start end)
     (clojure.lang.Range/create start end)))
  ([start end step]
   (if (and (instance? Long start) (instance? Long end) (instance? Long step))
     (clojure.lang.LongRange/create start end step)
     (clojure.lang.Range/create start end step))))
nil
```

Interesting!   So this function doesn't actually return a list, but a lazy `seq` abstraction - allowing me to lazily generate an infinite list of numbers.

## Let's learn some Clojure

Clojure has a handful of built-in data structures: lists, vectors, maps and sets.  Each has its own data literal notation.

Unlike classic Lisp, Clojure's core functions for collection handling interact with the `seq` abstraction, not directly with the list/vector/map/set type.

Let's make a list:

``` clojure 
(list 1 2 3)
```

``` shell
(1 2 3)
```

We can use a single quote as a short-cut, and check that it has the same result as the `list` function:

``` clojure
(= (list 1 2 3) '(1 2 3))
```

``` shell
true
```

Let's play with basic list operations:

``` clojure
(first '(1 2 3 4 5))
```

``` shell
1
```

``` clojure
(rest '(1 2 3 4 5))
```

``` shell
(2 3 4 5)
```

You add an element to the list with `conj`:

``` clojure
(conj '(1 2 3 4 5 6) 7)
```

``` shell
(7 1 2 3 4 5 6)
```

Notice that the list is updated from the front.

But this is functional programming - is the list actually *updated*?

``` clojure
(def list-1 '(1 2 3 4 5 6))
(conj list-1 7)
(list-1)
```

``` shell
(1 2 3 4 5 6)
```

No, Clojure is immutable by default (with clever structural sharing behind the scenes).

We can do the same operations on a vector:

``` clojure
(first [1 2 3 4 5])
```

``` shell
1
```

``` clojure
rest [1 2 3 4 5])
```

``` shell
(2 3 4 5)
```

``` clojure
(conj [1 2 3 4 5] 6)
```

``` shell
[1 2 3 4 5 6]
```

With a vector you can also index into its collection: 

``` clojure
(nth [1 2 3 4 5] 2)
```

``` shell
3
```

Maps are lists of key-value pairs.  You can use the `get` function to retrieve the value for a key.  

``` clojure
(get {:a 1 :b 2 :c 3} :a)
```

``` shell
1
```

``` clojure
(get {:a 1 :b 2 :c 3} :z)
```

``` shell
nil
```

If the keys in your map are keywords (indicated by an initial colon), keywords can act as functions and look themselves up in a map.

``` clojure
(:a {:a 1 :b 2 :c 3})
```

``` shell
1
```

Maps also can act as a function and look up a key.

``` clojure
user=> ({"a" 1 "b" 2 "c" 3} "a")
```

``` shell
1
```

We can add and remove key-value pairs in a map:

``` clojure
(assoc {:a 1 :b 2 :c 3} :d 4)
```

``` shell
{:a 1, :b 2, :c 3, :d 4}
````

``` clojure
(dissoc {:a 1 :b 2 :c 3} :c)
```

``` shell
{:a 1, :b 2}
```

We can merge maps together:

``` clojure 
(merge {:a 1 :b 2} {:b 7 :c 3} {:d 4 :e 5})
````

``` shell
{:a 1, :b 7, :c 3, :d 4, :e 5}
```

Let's try interacting with a set:

``` clojure 
(contains? #{1 2 3} 3)
```
``` shell
true
```

``` clojure 
(contains? #{1 2 3} 4)
```

``` shell
false
```

Maybe find the difference between two sets.  How do we do that?  Let's try `find-doc`: 

``` clojure
(find-doc "difference")
```

``` clojure
-------------------------
clojure.set/difference
([s1] [s1 s2] [s1 s2 & sets])
  Return a set that is the first set without elements of the remaining sets
-------------------------
clojure.core/unchecked-subtract
([x y])
  Returns the difference of x and y, both long.
  Note - uses a primitive operator subject to overflow.
-------------------------
clojure.core/unchecked-subtract-int
([x y])
  Returns the difference of x and y, both int.
  Note - uses a primitive operator subject to overflow.
nil
```

Pretty terse - the built-in Clojure documentation is more along the lines of a technical spec than a tutorial.

Let's try it:

``` clojure
(difference #{1 2 3} #{3 4 5})
```

``` java
CompilerException java.lang.RuntimeException: Unable to resolve symbol: difference in this context, compiling:(/private/var/folders/zy/8w41g9sd3wj5crqh_qnt5tz9zs3hh5/T/form-init7068970093664101222.clj:1:1)
```

Well, that ain't good.   A few interesting things to notice:

* Clojure error messages tend to look a little cryptic
* Clojure is not shy about its hosted nature
* Clojure is always _compiling_

Well, what does this error mean?  Clojure functions are defined in **namespaces**, and looking more closely I see that `difference` is part of the `clojure.set` namespace. 

Trying again:

``` clojure
(clojure.set/difference #{1 2 3 } #{3 4 5})
```

``` shell
#{1 2}
```

I'm a little hazy on my set operations - why are 1 and 2 returned, but not 4 and 5?  Let's look at the docs again:

``` clojure
clojure.set/difference
([s1] [s1 s2] [s1 s2 & sets])
  Return a set that is the first set without elements of the remaining sets
```

Terse but clear!

Let's check the ClojureDocs website to see if the community has contributed some examples for this particular function:
https://clojuredocs.org/clojure.set/difference

Let's try a couple functions for transforming an existing collection:

``` clojure
(filter even? (range 0 9))
```

``` shell
(0 2 4 6 8)
```

`map` is fun - you can define an anonymous function (using shorthand notation) to transform `list-1`: 


``` clojure
(map #(* 2 %) list-1)
```

``` shell
(2 4 6 8 10 12)
```

Let's define a custom function to capture this map operation.

``` clojure
(defn map-mult-2 [list]
    (map #(* 2 %) list))
```

Notice that a function is defined using a vector of arguments.  

That's one way that Clojure has improved a bit on classic Lisp - function definitions read a bit crisper since arguments stand out.

``` shell
#'user/map-mult-2
```

Now run it:

``` clojure 
(map-mult-2 '(1 2 3 4 5))
```

``` shell
(2 4 6 8 10)
```

Congratulations, you've learned most of Clojure.

Left for future learning:

* Let bindings and destructuring
* Concurrency constructs: atom, ref, agent and future
* `core.async`
* Macros
            
## Let's do some modeling

Both of these examples are taken from courses in Eric Normand's **PurelyFunctional.tv** [online mentoring for Clojure](https://purelyfunctional.tv). I think it's definitely worth signing up, at least for a month to try out
his [Introduction to Clojure](https://purelyfunctional.tv/courses/intro-to-clojure/).  

### Solitaire 

Clone this repo and follow along as I highlight Eric's use of TDD and refactoring in a functional code-base.

``` shell
git clone https://github.com/lispcast/solitaire.git
cd solitaire
``` 

